from django.shortcuts import render, redirect
from .models import Ajax, Commentary
import datetime
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from crud.forms import *
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

@login_required
def get_context_data(self, *args, **kwargs):
        comment_list = Ajax.objects.filter(pk=self.kwargs['pk'])
        return  render(request, 'ajax.html', {'comment_list': comment_list})

def ajax(request):
    if request.method == 'POST':
        ajax_list = Ajax.objects.order_by('task_name')
        if request.is_ajax():
            data = Ajax(
                task_name=request.POST['task_name'],
                status=request.POST['status'],
            )
            data.save()
            return JsonResponse({'data': 'success'})
    else:
        servidor = request.POST.get('task_name')
        comment_list = Commentary.objects.filter(pk=servidor).first()
        ajax_list = Ajax.objects.order_by('task_name')
        context = {'ajax_list': ajax_list}
    return render(request, 'ajax.html', {'ajax_list': ajax_list, 'comment_list': comment_list})


def comment(request):
    if request.method == 'POST':
        if request.is_ajax():
            servidor = request.POST['task_name']
            servidor = Commentary.objects.filter(pk=servidor).first()
            comment = request.POST['comment']
            data = Commentary(
                task_name=servidor,
                comment=comment,
            )
            data.save()

            return JsonResponse({'consulta': 'success'})
    else:
        comment_list = Commentary.objects.filter(pk=servidor).first()
        context = {'ajax_list': ajax_list}
    return render(request, 'ajax.html', {'ajax_list': ajax_list, 'comment_list': comment_list})

class TaskUpdateView(UpdateView):
    model = Ajax
    template_name = 'update.html'
    context_object_name = 'task'
    fields = ('task_name', 'status' )
    
    def get_success_url(self):
        return reverse_lazy('ajax')

def getajax(request):
    if request.method == 'GET':
        if request.is_ajax():
            data = Ajax.objects.order_by('task_name').first()
            datas = {"id": data.id, "task_name": data.task_name, "status": data.status}
            return JsonResponse(datas)
    else:
        return JsonResponse({'data': 'failure'})


@csrf_protect
def ajax_delete(request):
    if request.method == 'GET':
        if request.is_ajax():
            id = request.GET['id']
            ajax = Ajax.objects.get(id=id)
            ajax.delete()
            return JsonResponse({'data': 'success'})
    else:
        return JsonResponse({'data': 'failure'})



