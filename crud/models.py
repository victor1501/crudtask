from django.db import models
from django.core.validators import RegexValidator
from django.contrib.auth import get_user_model

USER = get_user_model()

STATUS_CHOICES = (
    ('OPEN', 'ABIERTO'),
    ('CLOSED', 'CERRADO'),
    ('IN_PROCESS', 'EN PROCESO'),
)


class Ajax(models.Model):
    task_name = models.CharField("Tarea", max_length=100)
    user_asig = models.ManyToManyField(USER)
    status = models.CharField(max_length=50, choices=STATUS_CHOICES)


class Commentary(models.Model):
    comment =   models.CharField("Comentario", max_length=100, blank=False)
    task_name = models.ForeignKey(Ajax, on_delete=models.CASCADE, blank=False)

